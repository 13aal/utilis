 * Utils Scripts
 ** mgtutils installer scripts


* Overview

Utilis is a program that was designed to make installing applications from mgtutils easy and effiecient. This program has a 32bit version, and a 64bit version. Both are required for the different operating systems. If you are running 32bit, you will need to use utilis_w32.bat, if you are running 64bit you will need to use utilis_w64.exe. In order for this program to work you must be logged into the users computer, or runas yourself. For more information either find your specified version, and run utilis -h or keep reading this help. Thank you.

* Basic usage

In order to run the scripts you will need to copy them over to the users computer
(to do this please see the functionality part of this file right under this one)
and run `.\utilis /?` this will output a list of flags that are available to be 
run, from there you can figure out what you need to do.

 - `.\utilis /bf` <- Will install the BigFix Client
 - `.\utilis /b` <- Will install Bomgar
 - `.\utilis /a` <- Will prompt you for what to install and install your desired
                    Adobe application
 - `.\utilis /c` <- Will install Cisco Anyconnect
 - `.\utilis /g` <- Will install Google Chrome
 - `.\utilis /d` <- Will prompt for a type of printer and find all the available
                    print drivers for that printer type
 - `.\utilis /r` <- Will install R and R studio
 - `.\utilis /t` <- Will install Tableau Reader or Desktop after prompted
 - `.\utilis /n` <- Will install NetScreen
 - `.\utilis /z` <- Will install ZoomText
 - `.\utilis /v` <- Will install VLC


* Functionality

These scripts are normal batch files that will be required on the users computer before it will work. There are multiple ways to do this:

  - (Dirty hack to transfer the files) You can backdoor into the users system
    and add the files to the users desktop by pushing `WIN+R` from your
    computer and doing `\\<COMPUTER-NAME>\c$` from there you can cd into the
    users desktop and copy the zip file over to the users computer.
    Extract the file and run the program as normal. Be sure you delete the 
    folder created when you are done, working on making the script delete
    itself (quickest and easiest way to install the application).
  - `Psexec` will allow you to run the scripts, you will need to run `psexec`
    with your oasam account as an admin, you will need to make sure that you
    have `cd`'d into the directory containing the script (this is SLOW).
    Specs: `psexec \\<COMP-NAME> -u oasam\<Z-ACCOUNT> -p <PASSWORD> -h -c ".\<SCRIPT-NAME> <FLAG>"`
    This will save the application into the users `system32` folder, so if needed
    it can be executed again from there.
  - You can send the files to the user and run them that way, sending the file
    will require you to create a trailing slash in the filename to allow the 
    file to be sent over the DOL network (dirty hack to trick the system).
    Once the file is sent you will need to run the file as an elevated user,
    no this does not mean run it in a admin cmd. Batch files run off of 
    group policy when sent over FTP and you will need to use the `runas`
    command, you can also send the entire directory as a zip file located
    in zip-archive directory. 
    Specs: `runas /user:oasam\<YOUR-Z-ACCOUNT> "cmd.exe <PATH-TO-FILE>"
  - I'm working on getting them added to `mgtutils` can't guarentee that
    will ever be there, but I'm working on it.


* Screeshots

Adobe:
https://s15.postimg.org/cggtki51n/adobe.png

BigFix Client:
https://s15.postimg.org/vzletv3t7/bigfix.png

Bomgar:
https://s15.postimg.org/pzxnq7j0r/bomgar.png

Cisco:
https://s15.postimg.org/mhlntti4r/cisco.png

Driver installation:
https://s15.postimg.org/eddjp2vpn/drivers.png


* Covered Applications/Programs/Services

 - Adobe Acrobat, Reader, Flash
 - Bomgar
 - Cisco Anyconnect
 - Google Chrome
 - BigFix Client
 - Print Drivers installation
 - R/R Studio
 - Tableau Reader and Desktop
 - Netscreen
 - ZoomText
 - VLC Media Player


* Agreements

Using these scripts or tricks for any other purpose is not a good idea.
Not only is it unethical it can compromise your job, and compromise myself.
If you decide you want to use any of the dirty hacks or any of the scripts
for any reason other then to install applications for a user. You and I will
have a problem. Furthermore by using these scripts you agree that you work
for the DOL IT Enterprise Service desk, and you are either a government contractor
or a government employee (IE NuAxis, Dell, etc..).


* Contributing

This program is in a version control system on BitBucket as a private 
repository. This means nobody can see it but the people that I choose, if you 
have some ideas, know batch, can write readable clean code, and want to help 
with this project, then get into contact with me via Skype, Dell or DOL side 
and we'll talk. These scripts MUST be written in batch, and must work simply.
