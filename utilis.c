/*


    Overview:
    ---------

Welcome to Utilis! This program was created to make your life easier while installing data from mgtutilis.

Now lets picture this really quick, you have to install a bunch of applications on a users system one by one,
well Utilis has all the main applications that are usually requested to be installed by users, Cisco, Bomgar,
Zoomtext, print drivers, etc..

Now you have to install all these apps but mgtutilis is slow as heck and will not load. Well there's two possible
issues there, either mgtutils is down, or it's just a slow server. This program relies heavily on mgtutils being
working, however, even if the connection is slow, it will still work. All you gotta do is use the correct flag that
will launch the installer script.



    How it works:
    ------------

Utilis works by running batch scripts that connect to \\mgtutils\windows7apps, after it has conencted successfully
it will begin copying the files required for the installation over to the users desktop, yes it has to copy the files.

After the files have been copied it's game time, it launches the application and begins the isntallation, once the
install has finished it will clean itself up by deleting all the files it just copied. Pretty nifty huh?



    Available flags:
    ---------------

As of now there are only 12 available flags that can be run (excluding bigfix (work in progress)). To see the full list
of flags and so you don't have to mess with reading this long drawn out thing, run utilis -h.

The flags are as follows:

-a which will install Adobe software (Reader, Acrobat, Flash) will be prompted for software type

-b which will install Bomgar as customer meaning they won't have rights to remote into people only accept remote support

-c which will install Cisco Anyconnect and setup the configuration for it

-g which will install Google Chrome from the registry, Chrome is in the DOL registry so you just have to find it

-n which will install Netscreen for MSHA users, simple, effective, and easy.

-d which will install print drivers for the users printer, will be prompted for printer type

-r which will install R and R studio, along with edit the environment variables and add R to the users PATH variable

-t which will install Tableau software (Desktop, Reader) will be prompted for software type

-v which will install VLC media player on the users system

-z which will install ZoomText on the users system

-h which will print the programs help page and exit the application, it will print a version of the help page that is
   not as defined as this version



    Acknowledgements:
    ----------------

I am the author and creator of Utilis. My name is Thomas Perkins and I work in the OKC call center for NuAxis. 

 * I would like to thank Tiffany Bailey for giving me the permissions to pursue this idea. Thank you for listening
   to my pitch on it and accepting the idea. Also thank you for allowing me to create this program.

 * I would personally like to thank Ricky Rowe for allowing me time to work and finish this project, without you sir this would 
   not have gone as smoothly nor would it be done as quickly as it was.
 
 * And finally I would like to thank Parima Pathipvanich for helping me to get into contact with the approval team. 
   I had no idea who to talk to about this and without you I would still have no idea. Thank you.


*/

#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>


int charCount(char *string, char *what)
{
    return *string == '\0' ? 0 : charCount(string + 1, what) + (*string == *what);
}


static void utilisBanner()
{
    // Sexy banner to be printed each time the program runs
    
    time_t rawtime;
    struct tm *timeInfo;
    char *version = "1.0"; // version number <major>.<minor>.<patch>.<commit-count>
    char *versionType;

    if(charCount(version, ".") == 1){
        versionType = "stable";
    } else {
        versionType = "dev";
    }


    time(&rawtime);
    timeInfo = localtime(&rawtime);

    printf("\n\n\n             .-') _                              .-')\n");
    printf("           (  OO) )                            ( OO ).\n");
    printf(" ,--. ,--.  /     '._ ,-.-')  ,--.      ,-.-') (_)---\\_)\n");
    printf(" |  | |  |  |'--...__)|  |OO) |  |.-')  |  |OO)/    _ |\n");
    printf(" |  | | .-')'--.  .--'|  |  \\ |  | OO ) |  |  \\\\  :` `.\n");
    printf(" |  |_|( OO )  |  |   |  |(_/ |  |`-' | |  |(_/ '..`''.)\n");
    printf(" |  | | `-' /  |  |  ,|  |_.'(|  '---.',|  |_.'.-._)   \\\n");
    printf("('  '-'(_.-'   |  | (_|  |    |      |(_|  |   \\       /\n");
    printf(" `-----'      `--'   `--'    `------'  `--'    `-----'\n");
    printf("v%s(%s)\n\n", version, versionType);
    printf("\n[*] Starting up on %s\n\n", asctime(timeInfo));

}


static void helpPage(char *programName)
{
    // Help page to be printed when no ARGV is given, or when -h is run

    printf("Usage CMD       : %s -[a|b|c|g|n|d|r|t|v|z|h]\nUsage Powershell: ./%s -[a|b|c|g|n|d|r|t|v|z|h]\n\n", programName, programName);
    printf(" Parameters:                    Descriptions:\n");
    printf("-------------                  ---------------\n");
    printf("    -a                      Install Adobe software\n");
    printf("    -b                    Install Bomgar as customer\n");
    printf("    -c                     Install Cisco Anyconnect\n");
    printf("    -g                       Install Google Chrome\n");
    printf("    -n                  Install Netscreen for MSHA users\n");
    printf("    -d                        Install print drivers\n");
    printf("    -r                       Install R and R studio\n");
    printf("    -t                      Install Tableau software\n");
    printf("    -v                      Install VLC media player\n");
    printf("    -z                          Install ZoomText\n");
    printf("    -h                      Print this help and exit\n");
    puts("\n");
}


void *launchInstaller(char *scriptPath, char *msgInfo)
{
    // Launch the installer scripts

    printf("Launching %s installer..\n", msgInfo);
    system(scriptPath);
}


char *strip(char *s)
{
    // Strip newline from a string
    // Example:

    // strip("test\n");
    // test

    return strtok(s, "\n");
}


void die(int exitCode, char *why)
{
    // Kill the program with an error message

    time_t rawtime;
    struct tm *timeInfo;
    time(&rawtime);
    timeInfo = localtime(&rawtime);
    int defaultExitCode = 0;
    
    if (exitCode > 3) {
        exitCode = defaultExitCode;
    }
    printf("\n\n[*] Shutting down on %s due to %s\n\n\n", strip(asctime(timeInfo)), why);
    exit(exitCode);
}


static void enumerate(int arrSize, char *data[])
{
    // Enumerate over an array and output the array in a numbered list
    // much like how Python's enumerate function works
    // Example:

    // char *friends[] = {"test", "test", "test"};
    // int length = sizeof(friends) / sizeof(*friends);
    // enumerate(length, friends);
    // # [1] test
    // # [2] test
    // # [3] test

    int i;
    for(i=0; i < arrSize; i++)
    {
        printf("[ %d ] %s\n", i+1, data[i]);
    }
}


int main(int argc, char *argv[])
{
    utilisBanner();
    bool isCaseSensitive = false;
    enum { CHARACTER_MODE, WORD_MODE, LINE_MODE };
    size_t optind;

    for (optind = 1; optind < argc && argv[optind][0] == '-'; optind++)
    {
        if (!argv[optind][1]){ // If no flag is thrown
            helpPage(argv[0]);
            die(1, "no valid flag was given");
        }

        switch(argv[optind][1])
        {
            case 'h': isCaseSensitive = true; helpPage("utilis"); exit(0);
            
            case 'a':
            {
                isCaseSensitive = true;
                char *adobeOpts[] = {"Adobe Acrobat", "Adobe Reader", "Adobe Flash Player", "Quit"};
                int adobeArrLngth = sizeof(adobeOpts) / sizeof(*adobeOpts);
                char choice;
                enumerate(adobeArrLngth, adobeOpts);
                printf("Enter your choice[1-%d]: ", adobeArrLngth);
                scanf("%c", &choice);
                
                switch(choice)
                {
                     // Have to use the computer code for the keys due to scanf()
 
                     case 49: launchInstaller("scripts\\adobe\\adobe.bat /a", "Adobe Acrobat"); // If you press 1

                     case 50: launchInstaller("scripts\\adobe\\adobe.bat /r", "Adobe Reader"); // if you press 2
  
                     case 51: launchInstaller("scripts\\adobe\\adobe.bat /f", "Adobe Flash Player"); // if you press 3

                     case 52: die(0, "exit requested by user."); // if you press 4
                
                }    
                    
             }
               // 1-9 -> 49-57

             case 'b': isCaseSensitive = true; launchInstaller("scripts\\Bomgar\\bomgar_install.bat", "Bomgar");

             case 'c': isCaseSensitive = true; launchInstaller("scripts\\Cisco_Anyconnect\\anyinstall.bat", "Cisco Anyconnect");

             case 'g': isCaseSensitive = true; launchInstaller("scripts\\Google_Chrome\\gchrome_install.bat", "Google Chrome");

             case 'n': isCaseSensitive = true; launchInstaller("scripts\\NetScreen\\nscreen_install.bat", "NetScreen");

             case 'd':
             {
                 isCaseSensitive = true;
                 char *driverOpts[] = {"Canon", "Xerox", "HP", "Dell", "Brother", "Ricoh", "Quit"};
                 int driverArrLngth = sizeof(driverOpts) / sizeof(*driverOpts);
                 char choice;
                 enumerate(driverArrLngth, driverOpts);
                 printf("Enter your choice[1-%d]: ", driverArrLngth);
                 scanf("%c", &choice);

                 switch(choice)
                 {
                     case 49: launchInstaller("scripts\\Print_Drivers\\driver.bat canon", "Canon print drivers");

                     case 50: launchInstaller("scripts\\Print_Drivers\\driver.bat xerox", "Xerox print drivers");

                     case 51: launchInstaller("scripts\\Print_Drivers\\driver.bat hp", "HP print drivers");

                     case 52: launchInstaller("scripts\\Print_Drivers\\driver.bat dell", "Dell print drivers");

                     case 53: launchInstaller("scripts\\Print_Drivers\\driver.bat brother", "Brother print drivers");

                     case 54: launchInstaller("scripts\\Print_Drivers\\driver.bat ricoh", "Ricoh print drivers");

                     case 55: die(0, "exit requested by user");
                 }
             }

             case 'r': isCaseSensitive = true; launchInstaller("scripts\\R\\r_install.bat", "R and R studio");

             case 't': 
             {
                 isCaseSensitive = true;
                 char *tabOpts[] = {"Tableau Desktop", "Tableau Reader", "Quit"};
                 int tabArrLngth = sizeof(tabOpts) / sizeof(*tabOpts);
                 char choice;
                 enumerate(tabArrLngth, tabOpts);
                 printf("Enter your choice[1-%d]: ", tabArrLngth);
                 scanf("%c", &choice);

                 switch(choice)
                 {
                     case 49: launchInstaller("scripts\\Tableau\\tab_install.bat /td", "Tableau Desktop");

                     case 50: launchInstaller("scripts\\Tableau\\tab_install.bat /tr", "Tableau Reader");

                     case 51: die(0, "exit was requested by user");
                 }
             }

            case 'v': isCaseSensitive = true; launchInstaller("scripts\\VLC_Player\\vlc_install.bat", "VLC Media Player");

            case 'z': isCaseSensitive = true; launchInstaller("scripts\\Zoomtext\\zt_install.bat", "Zoomtext");

            default:
                helpPage("utilis");
                exit(1); // [a|b|c|g|n|d|r|t|v|z|bf|h]
        }
    }
}
