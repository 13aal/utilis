To report a bug with utilis please do the following:

 - Copy the exact error message (if any) the command you ran
   and all the information from the command prompt that you can.
   (right click copy all, or scroll over all)
 - Create an email addressed - Utilis Script issue - 
 - Send to: perkins.thomas@dol.gov
   CC: bailey.tiffany.m@dol.gov;rowe.richard@dol.gov;pathipvanch.parima@dol.gov
 - Paste the output into the email and give a breif statement of what happened
 - Click send