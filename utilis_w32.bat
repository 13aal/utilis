:: :: :: :: :: :: ::
:: Author: Thomas Perkins
:: Description: Utilis is a program to install applications from mgtutils
:: Specs: .\utilis [PARAMS]
::


@echo off


:: Output the banner and the version number
:: Version number == <major>.<minor>.<patch>.<commit-count>
set "vnum=1.0"
if defined vnum (
   if "%vnum:~6%" neq "" if "%vnum:~7%" equ "" set "vtype=dev"
   if "%vnum:~2%" neq "" if "%vnum:~3%" equ "" set "vtype=stable"
)
@echo.
type "text_files\banner.txt"
@echo v%vnum%(%vtype%)
@echo. & echo.
@echo [*] Utilis starting up at %time%..
@echo. & echo.
goto :check_if_admin


:: Check if Utilis is being ran as administrator or not
:check_if_admin
    whoami /groups | findstr /b /c:"Mandatory Label\High Mandatory Level" | findstr /c:"Enabled group" > nul: && set IS_ELEVATED=1
    if %IS_ELEVATED==0 (
        set /p _="You need to run Utilis as administrator, run with admin cmd. Press enter to exit.."
    ) else (
        :: Check if an argument variable is given or not
        if [%1]==[] (
            goto :help_menu
        ) else (
            goto :verify_integrity
        )
    )


:: Verify the integrity of the program
:verify_integrity
    set "unverified_warn_message=*** CHECKSUM VERIFICATION HAS FAILED. YOU ARE RUNNING AN UNVERIFIED VERSION OF UTILIS. ***"
    set "verf_integrity_src_file=\\ROCIO-148084\c$\Users\z-perkins-thomas\Documents\bin\batch\verf\checksum.md5"
    set "verf_integrity_dst_file=%cd%\docs\checksum.md5"
    fc "%verf_integrity_src_file%" "%verf_integrity_dst_file%" >nul || @echo %unverified_warn_message% & echo.


:: verify what to install if an argument variable is given
:verify_argv
    if '%1'=='/?' goto :help_menu
    if '%1'=='/a' set "adobe_path=%cd%\scripts\Adobe" & goto :adobe_install
    if '%1'=='/b' set "bomgar_path=%cd%\scripts\Bomgar" & goto :bomgar_install
    if '%1'=='/c' set "cisco_path=%cd%\scripts\Cisco_Anyconnect" & goto :cisco_install
    if '%1'=='/d' set "driver_path=%cd%\scripts\Print_Drivers" & goto :driver_install
    if '%1'=='/g' set "chrome_path=%cd%\scripts\Google_Chrome" & goto :chrome_install
    if '%1'=='/n' set "nscreen_path=%cd%\scripts\NetScreen" & goto :netscreen_install
    if '%1'=='/r' set "r_path=%cd%\scripts\R" & goto :r_install
    if '%1'=='/t' set "tab_path=%cd%\scripts\Tableau" & goto :tableau_install
    if '%1'=='/v' set "vlc_path=%cd%\scripts\VLC_Player" & goto :vlc_install
    if '%1'=='/z' set "zt_path=%cd%\scripts\ZoomText" & goto :zt_installation
    if '%1'=='/bf' set "bf_path=%cd%\scripts\BigFix_Client" & goto :bigfix_install


:: Program help menu
:help_menu
    @echo     Install end user software from \\mgtutils01\windows7apps
    @echo.
    @echo                  Usage: .\utilis [PARAMS]
    @echo.
    @echo    Parameters                         Description
    @echo  --------------------------------------------------------------------------
    @echo       /a               Install Adobe software (Reader, Acrobat, Flash)
    @echo       /b               Install Bomgar as a customer
    @echo       /c               Install Cisco Anyconnect
    @echo       /d               Install a print driver (HP, Ricoh, Brother, etc..)
    @echo       /r               Install R and R Studio
    @echo       /g               Install Google Chrome
    @echo       /t               Install Tableau software (Reader, Desktop)
    @echo       /n               Install NetScreen for MSHA users
    @echo       /z               Install ZoomText
    @echo       /v               Install VLC Media Player
    @echo       /bf              Install the BigFix Client
    @echo      "/?"              Run this help and exit
    @echo.
    :: Had to use "/?" or else it will tell you what echo does
    set /p _="Press enter to continue.."
    exit


:: Install Tableau Reader or Desktop
:tableau_install
    echo 1. Tableau Reader & echo 2. Tableau Publid Desktop & echo 3. Exit
    set /p choice="Which would you like to install[1-3]: "
    if %choice%==1 (
        call "%tab_path%\tab_install.bat" /tr
    ) else if %choice%==2 (
        call "%tab_path%\tab_install.bat" /td
    ) else if %choice%==3 (
        exit
    ) else (
        cls
        @echo Invalid choice..
        goto :tableau_install
    )


:: Install adobe on the users computer
:adobe_install
    echo 1. Adobe Reader & echo 2. Adobe Flash Player & echo 3. Adobe Acrobat & echo 4. Exit
    set /p choice="What would you like to install[1-4]: "
    if %choice%==1 (
        echo Running reader installer.. & call "%adobe_path%\adobe.bat" /r
    ) else if %choice%==2 (
        echo Running flash installer.. & call "%adobe_path%\adobe.bat" /f
    ) else if %choice%==3 (
        echo Running acrobat installer.. & call "%adobe_path%\adobe.bat" /a
    ) else if %choice%==4 (
        exit
    ) else (
        cls
        @echo Invalid choice..
        goto :adobe_install
    )


:: Install a print driver on the users system
:driver_install
    echo 1. HP & echo 2. Xerox & echo 3. Canon & echo 4. Dell & echo 5. Brother & echo 6. Ricoh & echo 7. Exit
    @echo. & set /p printer_type="What type of printer are they using[1-7]: "
    if %printer_type%==1 (
        call "%driver_path%\driver.bat" hp
    ) else if %printer_type%==2 (
        call "%driver_path%\driver.bat" xerox
    ) else if %printer_type%==3 (
        call "%driver_path%\driver.bat" canon
    ) else if %printer_type%==4 (
       call "%driver_path%\driver.bat" dell
    ) else if %printer_type%==5 (
       call "%driver_path%\driver.bat" brother
    ) else if %printer_type%==6 (
       call "%driver_path%\driver.bat" ricoh
    ) else if %printer_type%==7 (
        exit
    ) else (
        cls
        @echo Invalid choice...
        goto :driver_install
    )


:: Install BigFix Client on the users computer
:bigfix_install
    echo Running BigFix installation..
    call "%bf_path%\bf_install.bat"


:: Install Bomgar on the users system
:bomgar_install
    echo Running Bomgar installation..
    call "%bomgar_path%\bomgar_install.bat"


:: Install Cisco Anyconnect on the users computer
:cisco_install
    echo Running Cisco Anyconnect installation..
    call "%cisco_path%\anyinstall.bat"


:: Install R language and R studio on the users system
:r_install
   echo Running R lang and R studio installation..
   call "%r_path%\r_install.bat"


:: Install Google Chrome on the users system
:chrome_install
    echo Running Google Chrome installation..
    call "%chrome_path%\gchrome_install.bat"


:: Install NetScreen for MSHA users, must run as admin
:netscreen_install
    echo Running NetScreen installation..
    call "%nscreen_path%\nscreen_install.bat"


:: Install ZoomText on the users system, must run as admin
:zt_installation
    echo Running ZoomText installation..
    call "%zt_path%\zt_install.bat"


:: Install VLC Media Player
:vlc_install
    echo Running VLC Player installer..
    call "%vlc_path%\vlc_install.bat"
