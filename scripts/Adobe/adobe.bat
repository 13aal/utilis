:: :: :: ::
:: Author: Thomas Perkins
:: Description: Install adobe software on the users computer
:: Specs: .\adobe /[PARAMS]
::

@echo off


:: verify what to install
:verify_argv
	if '%1'=='/a' goto :acro_install
	if '%1'=='/r' goto :reader_install
	if '%1'=='/f' goto :flash_install


:: install adobe acrobat
:acro_install
	set adobe_path=\\mgtutils01\windows7apps\Suite Packaged SW (Adobe and Microsoft)\Adobe Master Folder\Adobe Reader 11.0.19
	set exectuable_name=AcroRead.msi
	pushd %adobe_path%
	@echo %copy_info_log%
	xcopy "%adobe_path%\%exectuable_name%" "%USERPROFILE%\Desktop"
	@echo Installing Adobe Acrobat quietly..
	msiexec /i "%USERPROFILE%\Desktop\%exectuable_name%" /QN /L*V "%USERPROFILE%\AppData\Local\adobe_acro_log.txt"
    pause
    DEL "%USERPROFILE%\Desktop\%exectuable_name%"
    @echo Done, go to "%USERPROFILE%\AppData\Local\adobe_reader_log.txt" for verbose output.


:: Install adobe reader
:reader_install
	set adobe_path=\\mgtutils01\windows7apps\Suite Packaged SW (Adobe and Microsoft)\Adobe Master Folder\Adobe Reader 11.0.19
	set exectuable_name=AdbeRdrUpd11019.msp
	pushd %adobe_path%
	@echo %copy_info_log%
	xcopy "%adobe_path%\%exectuable_name%" "%USERPROFILE%\Desktop"
	@echo Installing Adobe Reader quietly..
	msiexec /p "%USERPROFILE%\Desktop\%exectuable_name%" /QN /L*V "%USERPROFILE%\AppData\Local\adobe_reader_log.txt"
    pause
    DEL "%USERPROFILE%\Desktop\%exectuable_name%"
    @echo Done, go to "%USERPROFILE%\AppData\Local\adobe_acro_log.txt" for verbose output.


:: Install adobe flash player
:flash_install
	set adobe_path="\\mgtutils01\windows7apps\Suite Packaged SW (Adobe and Microsoft)\Adobe Master Folder\Adobe Flash Player\21.0.0.213"
	set plugin_exec_name=install_flash_player_21_plugin.msi
	set exectuable_name=install_flash_player_21_active_x.msi
	set /p _="Inform user that all browser tasks are about to be closed, press enter when ready to begin.."
	taskkill /f /im FIREFOX.exe /t
	taskkill /f /im IEXPLORER.exe /t
	taskkill /f /im CHROME.exe /t
	:: kill all browser processes
	pushd %adobe_path%
	@echo Copying %exectuable_name% to %USERPROFILE%\Desktop, this could take a few minutes..
	xcopy %adobe_path%"\%exectuable_name%" "%USERPROFILE%\Desktop"
	@echo Copying %plugin_exec_name% to the same place, please wait..
	xcopy %adobe_path%"\%plugin_exec_name%" "%USERPROFILE%\Desktop"
	@echo Installing Flash Player quietly..
	msiexec /i "%USERPROFILE%\Desktop\%exectuable_name%" /QN /L*V "%USERPROFILE%\AppData\Local\adobe_flash_log.txt"
	@echo Installing plugin..
	msiexec /i "%USERPROFILE%\Desktop\%plugin_exec_name%" /QN /L*V "%USERPROFILE%\AppData\Local\adobe_flash_plugin_log.txt"
    pause
    @echo Done, see verbose output at "%USERPROFILE%\AppData\Local".
    DEL "%USERPROFILE%\Desktop\%exectuable_name%"
