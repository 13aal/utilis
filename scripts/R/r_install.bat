:: :: :: :: :: ::
:: Author: Thomas Perkins
:: Description: Install R and R studio
:: Specs: .\r_install
::

@echo off

:: Set the variables
set "r_path=\\mgtutils01\Windows7Apps\Statistical SW\R and R Studio"
set "r_lang_exec=R-3.3.2-win (For R Studio).exe"
set "r_studio_exec=RStudio v1.0.136 (Requires R).exe"

:: Push into mgtutils
pushd "%r_path%"

:: Copy the files to the desktop
@echo "Copying '%r_lang_exec%' and '%r_studio_exec%' to %USERPROFILE%\Desktop, this may take a few minutes.."
xcopy "%r_lang_exec%" "%USERPROFILE%\Desktop"
xcopy "%r_studio_exec%" "%USERPROFILE%\Desktop"

:: Start the executables
@echo "Executing the programs, follow the prompts.."
start /wait "" "%USERPROFILE%\Desktop\%r_lang_exec%"
start /wait "" "%USERPROFILE%\Desktop\%r_studio_exec%"

:: Delete the exec's from users Desktop
pause
DEL "%USERPROFILE%\Desktop\%r_lang_exec%"
DEL "%USERPROFILE%\Desktop\%r_studio_exec%"

:: Get rid of the created drive path
POPD
