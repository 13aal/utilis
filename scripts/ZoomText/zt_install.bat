@echo off

set "zt_path=\\mgtutils01\Windows7Apps\Accessibility SW\ZoomText\ZoomText 10 for ODEP"
set "zt_exec_name=setup.exe"
goto :check_if_admin

:: Check if your running the terminal as administrator or not.
:check_if_admin
    set IS_ELEVATED=0
    whoami /groups | findstr /b /c:"Mandatory Label\High Mandatory Level" | findstr /c:"Enabled group" > nul: && set IS_ELEVATED=1
    :: Output that you need to run as admin, and exit
    if IS_ELEVATED==0 set /p _="This application can only be installed as an administrator. Run an adminstrator command prompt, press enter to exit.."
    :: Continue the process
    if IS_ELEVATED==1 goto :install_zoom_text 

:install_zoom_text
    pushd "%zt_path%"
    echo. & @echo Copying exectuable to %USERPROFILE%\Desktop, please wait.. & echo.
    :: Copy yhe executable..
    xcopy "%zt_exec_name%" "%USERPROFILE%\Desktop"
    :: Launch the exectuable..
    start /wait "" "%USERPROFILE%\Desktop\%zt_exec_name%"
    echo.
    set /p _="Delete the executable from the users Desktop, press enter when done.."