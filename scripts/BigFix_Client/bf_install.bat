::::::::
:: Author: Thomas Perkins
:: Date: Created on 03/14/2017
:: Usage: .\bf_install


@echo off


:: Set variables and paths
set "source_dir=\\mgtutils01\windows7apps\BigFix client\Bigfix Client 9.2.7.53"
set "exec_name=BigfixAgent.msi"
set "log_path=%USERPROFILE%\AppData\Local\bigfix_log.txt"
pushd "%source_dir%"


:: Copy the MSI to the desktop
@echo "Copying %exec_name% to %USERPROFILE%\Desktop, please wait.."
xcopy "%source_dir%\%exec_name%" "%USERPROFILE%\Desktop"


:: Silently install BigFix Client
@echo "Installing %exec_name% quietly check %log_path% for verbose output.."
msiexec /i "%USERPROFILE%\Desktop\%exec_name%" /QN /L*V "%log_path%"


:: Clean up
pause
@echo "Done installing, cleaning up.."
DEL "%USERPROFILE%\Desktop\%exec_name%"
set /p _="%exec_name% has been installed, pres enter to exit.."
