::::::::
:: Created by Thomas Perkins 02/22/2017
:: Program created to easily install Cisco Anyconnect for a user
:: Version: 0.1

@echo off

pushd "\\mgtutils01\windows7apps\Communications SW\Cisco\Cisco AnyConnect\InstallAnyconnect4.2"
:: push into mgtutils
@echo "Running installer script.."
start /wait anyconnect.bat
set /p _="Any connect has been installed, press enter to exit.."
