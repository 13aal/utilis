:: :: :: ::
:: Author: Thomas Perkins
:: Description: Install Bomgar on a users computer quietly and write verbose output to a file
:: Specs: .\bomgar_install
::


@echo off


:: query the registry for the operating system architecture (64 or 32 bit)
reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32 || set OS=64


:: Set the variables based on the users operating system
set "src_dir_path=\\mgtutils01\Windows7Apps\Bomgar Clients\Bomgar Customer %OS%-bit"
set "exec_name=bomgar-elvsvc-win%OS%.msi"


echo Copying '%OS%bit' version of Bomgar to '%USERPROFILE%\Desktop', please wait..
pushd "\\mgtutils01\Windows7Apps"
xcopy /s/z "%src_dir_path%\%exec_name%" "%USERPROFILE%\Desktop"
echo Copied successfully, launching installer..


:: launch the msi silently
msiexec /i "%USERPROFILE%\Desktop\%exec_name%" /QN /L*V "%USERPROFILE%\AppData\Local\bomgar_log.txt"
set /p _="Installed successfully, for verbose logging see %USERPROFILE%\AppData\Local\bomgar_log.txt, press enter to exit"
