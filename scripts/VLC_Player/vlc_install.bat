@echo off


goto :install_vlc

:: Install VLC media player
:install_vlc
    :: Query registry for 32bit or 64bit OS
    reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32 || set OS=64
    set "vlc_path=\\mgtutils01\Windows7Apps\Multimedia SW\VLC\VLC Player 2.2.0\%OS%-bit"
    set "exec_name=vlc-2.2.0-win%OS%.exe"
    @echo. & @echo Copying %OS%bit installer to %USERPROFILE%\Desktop, please wait.. & @echo.
    pushd "%vlc_path%"
    xcopy "%exec_name%" "%USERPROFILE%\Desktop"
    start /wait "" "%USERPROFILE%\Desktop\%exec_name%"
    pause
    DEL "%USERPROFILE%\Desktop\%exec_name%"
    POPD
