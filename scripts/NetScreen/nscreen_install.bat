@echo off


set "netscreen_path=\\mgtutils01\Windows7Apps\NetScreen VPN Client"
set "nscreen_exec=Netscreen.exe"

goto :install_netscreen

:install_netscreen
        pushd "%netscreen_path%"
        @echo Copying %nscreen_exec% to %USERPROFILE%\Desktop, please wait..
        xcopy "%nscreen_exec%" "%USERPROFILE%\Desktop"
        start "" /wait "%USERPROFILE%\Desktop\%nscreen_exec%"
        pause
        DEL "%USERPROFILE%\Desktop\%nscreen_exec%"
        POPD
