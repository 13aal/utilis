@echo off

goto :verify_argv

:verify_argv
    if '%1'=='/td' set "tab_desk_path=\\mgtutils01\Windows7Apps\Data Management SW\Tableau Public" & goto :tab_public_desktop_install
    if '%1'=='/tr' set "tab_read_path=\\mgtutils01\Windows7Apps\Data Management SW\Tableau Reader\Tableau Reader v10.2" & goto :tab_reader_install

:tab_reader_install
    :: Query the registry for either 64bit or 32bit operation system
    reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32 || set OS=64
    :: Set the executable name according to the query
    if %OS%==64 set "exec_name=TableauReader-64bit-10-2-0.exe"
    if %OS%==32 set "exec_name=TableauReader-32bit-10-2-0.exe"
    :: Copy the executable
    pushd "%tab_read_path%"
    xcopy "%exec_name%" "%USERPROFILE%\Desktop"
    :: Execute
    start /wait "" "%USERPROFILE%\Desktop\%exec_name%"
    :: Delete the created drive and executable
    DEL "%USERPROFILE%\Desktop\%exec_name%"
    POPD

:tab_public_desktop_install
    :: Set the executable name, there's only a 64bit
    set "exec_name=Tableau Public Desktop v10.2 (x64).exe"
    :: Push into mgtutils
    pushd "%tab_desk_path%"
    :: Copy the executable to the users desktop
    xcopy "%exec_name%" "%USERPROFILE%\Desktop"
    :: Run the process
    start /wait "" "%USERPROFILE%\Desktop\%exec_name%"
    :: Delete the created drive and the executable from the desktop
    pause
    DEL "%USERPROFILE%\Desktop\%exec_name%"
    POPD
