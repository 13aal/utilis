::::::::::
::
:: Author: Script created on 02/28/2017 by: Thomas Perkins
:: Specs: Script will run a Google Chrome installer depending
::        on the users operating system, 64bit or 32bit
:: Usage: gchrome_install.bat
::
::

@echo OFF

regedit /add "%~dp0UpdateCheck.reg" /f
"C:\Users\Public\Desktop\Google Chrome.lnk"
exit