::::::::
:: Author: Thomas Perkins
:: Description: Install print drivers on a users computer
:: Specs: .\driver [PRINTER TYPE]
::

@echo off

set "src_dir=\\mgtutils01\windows7apps\PRINTERS"
goto :verify_argv

:: verify what type of print driver needs to be installed
:verify_argv
    IF '%1'=='canon' set "dir_path=%src_dir%\Canon" & GOTO :install_drivers
    IF '%1'=='xerox' set "dir_path=%src_dir%\Xerox" & goto :install_drivers
    IF '%1'=='hp' set "dir_path=%src_dir%\HP" & goto :install_drivers
    IF '%1'=='dell' set "dir_path=%src_dir%\Dell" & goto :install_drivers
    IF '%1'=='brother' set "dir_path=\\mgtutils01\windows7apps\Brother\Drivers" & goto :install_drivers
    IF '%1'=='ricoh' set "dir_path=\\mgtutils01\windows7apps\Ricoh" & goto :install_drivers
    goto :eof


:install_drivers
    @echo. & @echo Finding drivers for %1% devices... & @echo.
    pushd "%dir_path%"
    :: push into the mgtutils directory
    ::forfiles /s /m *.exe /c "cmd /c echo @relpath"
    dir /s/b/a *.exe
    :: find all the drivers of the given
    @echo. & set /p to_install="Copy the path of the correct driver and paste here: "
    @echo. & @echo Copying file to %USERPROFILE%, please wait this may take a few minutes.. & @echo.
    xcopy %to_install% "%USERPROFILE%\Desktop"
    :: copy the driver to the users Desktop
    for %%f in (%to_install%) do set "exec_name=%%~nxf"
    :: grab the executable name from the path
    @echo. & @echo Installing %exec_name%.. & @echo.
    pushd "%USERPROFILE%\Desktop"
    start /wait "" "%exec_name%"
    :: execute the executable
    set /p _="%exec_name% has run successfully press enter to exit.."
    exit
